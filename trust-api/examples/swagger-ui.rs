use actix_web::{App, HttpServer};
use std::{error::Error, net::Ipv4Addr};
use trust_api::TrustApiDoc;
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

#[actix_web::main]
async fn main() -> Result<(), impl Error> {
    // Serve up swagger UI on localhost:8080
    HttpServer::new(move || {
        App::new().service(SwaggerUi::new("/{_:.*}").url("/api-doc/openapi.json", TrustApiDoc::openapi()))
    })
    .bind((Ipv4Addr::UNSPECIFIED, 8080))
    .unwrap()
    .run()
    .await
}
