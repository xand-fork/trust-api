use trust_api::TrustApiDoc;
use utoipa::OpenApi;

fn main() {
    let api = TrustApiDoc::openapi();

    let json_api = api.to_pretty_json().expect("Failed to write json");

    // Print generated swagger doc to console
    println!("{}", json_api);
}
