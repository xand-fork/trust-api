use trust_api::TrustApiDoc;
use utoipa::OpenApi;

fn main() {
    let api = TrustApiDoc::openapi();

    let yaml_api = api.to_yaml().expect("Failed to write yaml");

    // Print generated swagger doc to console
    println!("{}", yaml_api);
}
