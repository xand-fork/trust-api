#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    clippy::unreadable_literal,
    rust_2018_idioms
)]
// Safety-critical application lints
#![deny(clippy::pedantic, clippy::float_cmp_const, clippy::unwrap_used)]
#![allow(
    clippy::implicit_return,
    clippy::iter_nth_zero,
    clippy::match_bool,
    clippy::missing_errors_doc,
    clippy::non_ascii_literal,
    clippy::wildcard_imports,
    incomplete_features,
    clippy::module_name_repetitions
)]
// To use the `unsafe` keyword, do not remove the `unsafe_code` attribute entirely.
// Instead, change it to `#![allow(unsafe_code)]` or preferably `#![deny(unsafe_code)]` + opt-in
// with local `#[allow(unsafe_code)]`'s on a case-by-case basis, if practical.
#![forbid(unsafe_code, bare_trait_objects)]
// Uncomment before ship to reconcile use of possibly redundant crates, debug remnants, missing
// license files and more
// #![allow(clippy::blanket_clippy_restriction_lints)]
// #![warn(clippy::cargo, clippy::restriction, missing_docs, warnings)]
// #![allow(clippy::implicit_return)]
#![cfg_attr(test, allow(non_snake_case, clippy::unwrap_used))]

mod i_trust_api;
mod web_server;

mod error;
mod impl_trust_api;

use trust_api_config::{load_config, TrustApiConfig};

pub use error::{Error, Result};
pub use i_trust_api::{
    i_service::{IService, ServiceStatus},
    ITrustApi, TotalClaims,
};
pub use web_server::TrustApiDoc;

/// The entrypoint into the Trust API service.
/// This function will read in trust configuration from the provided path and use it to spin up
/// a trust API server connected to a running Xand API service.
/// A note on "`allow(clippy::future_not_send)`":
///   This crate explicitly enables the `future_not_send` lint, but it is impossible to maintain that
///   through the entry point due to Actix's `HttpServer` not being Send. See top of file for context.
#[allow(clippy::future_not_send)]
pub async fn lib_main(config_path: &str) -> Result<()> {
    let config: TrustApiConfig = load_config(config_path)?;
    web_server::start_server(&config).await
}
