use clap::Parser;
use trust_api::{lib_main, TrustApiDoc};
use utoipa::OpenApi;

const OUTPUT_DIR: &str = "build";
const OUTPUT_FILE: &str = "trust-api";
const OUTPUT_EXT: &str = "yaml";

#[actix_rt::main]
async fn main() {
    let cli = Cli::parse();

    match cli.generate {
        true => generate(OUTPUT_DIR, OUTPUT_FILE, OUTPUT_EXT),
        false => lib_main(&cli.config_path)
            .await
            .expect("Failed to run trust API server"),
    };
}

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    #[clap(short, long)]
    generate: bool,

    #[clap(short, long, default_value = "/etc/xand/trust-api/config/config.yaml")]
    config_path: String,
}

/// Generates an OpenAPI document representation of the web server
fn generate(output_dir: &str, output_file: &str, output_ext: &str) {
    let api = TrustApiDoc::openapi();
    let output_path = format!("{}/{}-{}.{}", output_dir, output_file, api.info.version, output_ext);
    println!("Generating OpenAPI yaml document to {}...", output_path);
    let yaml_api = api.to_yaml().expect("Failed to write yaml");
    if let Err(e) = std::fs::create_dir(output_dir) {
        println!("Error while creating build folder: {:?}", e);
    };
    std::fs::write(output_path.clone(), yaml_api).expect("Failed to write file for OpenAPI Doc");
    println!("Successfully wrote OpenAPI yaml document at {}", output_path);
}
