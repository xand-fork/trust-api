use crate::{
    i_trust_api::{error::Result, ITrustApiFactory},
    IService, ITrustApi, TotalClaims,
};

use async_trait::async_trait;
use trust_api_config::TrustApiConfig;
use xand_api_client::ReconnectingXandApiClient;
use xand_api_client::TotalIssuance;

#[async_trait]
pub trait XandApiService: Send + Sync + IService {
    async fn get_total_issuance(&self) -> Result<TotalIssuance>;
}

pub struct TrustApiService<X: XandApiService> {
    pub(crate) xand_api_client: X,
}

#[async_trait]
impl<X: XandApiService> ITrustApi for TrustApiService<X> {
    async fn get_total_claims(&self) -> Result<TotalClaims> {
        let total_issuance = self.xand_api_client.get_total_issuance().await?;
        Ok(total_issuance.try_into()?)
    }

    fn services(&self) -> Vec<&dyn IService> {
        vec![&self.xand_api_client]
    }
}

impl ITrustApiFactory for TrustApiConfig {
    fn generate_service(&self) -> Box<dyn ITrustApi> {
        Box::new(TrustApiService {
            xand_api_client: ReconnectingXandApiClient::stub_jwt(
                self.xand_api.url.parse().expect("Malformed Xand API url provided."),
                self.xand_api.jwt.clone(),
            ),
        })
    }
}

#[cfg(test)]
mod tests;
mod xand_api_service;
