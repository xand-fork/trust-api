#[cfg(test)]
mod tests;

mod trust_api_doc;

pub use trust_api_doc::TrustApiDoc;

use crate::{
    error::Result,
    i_trust_api::{ITrustApi, ITrustApiFactory},
};
use actix_web::{
    body::BoxBody,
    dev::{ServiceFactory, ServiceRequest, ServiceResponse},
    web::{Data, ServiceConfig},
    App, Error, HttpServer,
};
use std::net::Ipv4Addr;
use trust_api_config::ITrustApiConfig;
use trust_api_doc::{services_api::get_service_health, total_claims_api::get_total_claims};
use utoipa::{openapi, OpenApi};
use utoipa_swagger_ui::SwaggerUi;

/// Provides an entrypoint to the Trust API functionality for the implementation of the app
pub struct ServerState {
    pub service: Box<dyn ITrustApi>,
}

/// Adds the `ServerState` as application data and configures the defined routes to the app.
fn configure(server_state: Data<ServerState>) -> impl FnOnce(&mut ServiceConfig) {
    |config: &mut ServiceConfig| {
        config
            .app_data(server_state)
            .service(get_service_health)
            .service(get_total_claims);
    }
}

// A helper function that extracts out the actix app builder functionality. It is extracted for testing
// purposes
fn build_web_app(
    server_state: Data<ServerState>,
    open_api: openapi::OpenApi,
) -> App<
    impl ServiceFactory<ServiceRequest, Config = (), Response = ServiceResponse<BoxBody>, Error = Error, InitError = ()>,
> {
    App::new()
        .configure(configure(server_state))
        .service(SwaggerUi::new("/{_:.*}").url("/api-doc/openapi.json", open_api))
}

/// Starts the Trust API as a served up swagger ui configured by the configuration parameter
// This crate explicitly enables the future_not_send lint, but it is impossible to maintain that
// through the entry point due to Actix's HttpServer not being Send. See lib.rs for context.
#[allow(clippy::future_not_send)]
pub async fn start_server<Config: ITrustApiConfig + ITrustApiFactory>(config: &Config) -> Result<()> {
    let server_state = Data::new(ServerState {
        service: config.generate_service(),
    });
    let open_api = TrustApiDoc::openapi();
    let port = config.port();

    HttpServer::new(move || build_web_app(server_state.clone(), open_api.clone()))
        .bind((Ipv4Addr::UNSPECIFIED, port))?
        .run()
        .await
        .map_err(Into::into)
}
