use crate::i_trust_api::error::Error as ITrustApiError;
use thiserror::Error;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Couldn't convert timestamp")]
    InvalidTimeConversion,
}

impl From<Error> for ITrustApiError {
    fn from(error: Error) -> Self {
        Self::InternalServiceError(Box::new(error))
    }
}
