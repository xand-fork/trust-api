pub mod error;

use async_trait::async_trait;
use chrono::{DateTime, LocalResult, TimeZone, Utc};

use crate::i_trust_api::error::Result as ITrustApiResult;
use crate::impl_trust_api::xand_api_service::error::{Error as XandApiServiceError, Result as XandApiServiceResult};
use crate::{impl_trust_api::XandApiService, IService, ServiceStatus, TotalClaims};
use xand_api_client::{ReconnectingXandApiClient, TotalIssuance, XandApiClientTrait};

#[async_trait]
impl XandApiService for ReconnectingXandApiClient {
    async fn get_total_issuance(&self) -> ITrustApiResult<TotalIssuance> {
        XandApiClientTrait::get_total_issuance(self).await.map_err(Into::into)
    }
}

#[async_trait]
impl IService for ReconnectingXandApiClient {
    fn name(&self) -> String {
        "Xand API Service".to_string()
    }

    async fn health(&self) -> ServiceStatus {
        if let Err(e) = self.get_current_block().await {
            ServiceStatus::Down { details: e.to_string() }
        } else {
            ServiceStatus::Up
        }
    }
}

impl TryFrom<TotalIssuance> for TotalClaims {
    type Error = XandApiServiceError;

    fn try_from(x: TotalIssuance) -> XandApiServiceResult<Self> {
        let total_claims_in_minor_units = x.total_issued;
        let finalized_block_id = x.blockstamp.block_number;

        let timestamp: DateTime<Utc> = match Utc.timestamp_millis_opt(x.blockstamp.unix_timestamp_ms) {
            LocalResult::None | LocalResult::Ambiguous(_, _) => Err(XandApiServiceError::InvalidTimeConversion),
            LocalResult::Single(date_time) => Ok(date_time),
        }?;

        Ok(Self {
            total_claims_in_minor_units,
            finalized_block_id,
            timestamp,
        })
    }
}
