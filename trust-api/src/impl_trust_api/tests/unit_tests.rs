use super::*;
use crate::{
    impl_trust_api::tests::fakes::{FakeXandApiClient, Health},
    ServiceStatus,
};
use chrono::{DateTime, Utc};
use std::time::{Duration, UNIX_EPOCH};
use xand_api_client::Blockstamp;

#[allow(clippy::cast_sign_loss)]
fn time_into_datetime(time: i64) -> DateTime<Utc> {
    let d = UNIX_EPOCH + Duration::from_millis(time as u64);
    DateTime::<Utc>::from(d)
}

#[tokio::test]
async fn get_total_claims__returns_expected_total() {
    // given
    let total_issued = 10000;
    let block_number = 2999;
    let unix_timestamp_ms = 9_378_483;
    let total_issuance = TotalIssuance {
        total_issued,
        blockstamp: Blockstamp {
            block_number,
            unix_timestamp_ms,
            is_stale: false,
        },
    };
    let xand_api_client = FakeXandApiClient {
        total_issuance: Some(total_issuance),
        health: None,
    };
    let service = TrustApiService { xand_api_client };
    let timestamp = time_into_datetime(unix_timestamp_ms);

    let expected = TotalClaims {
        total_claims_in_minor_units: total_issued,
        finalized_block_id: block_number,
        timestamp,
    };

    // when
    let actual = service.get_total_claims().await.unwrap();

    // then
    assert_eq!(actual, expected);
}

#[tokio::test]
async fn services__correctly_reports_service_up() {
    // given
    let xand_api_client = FakeXandApiClient {
        total_issuance: None,
        health: Some(Health {
            service_status: ServiceStatus::Up,
            name: "Fake Xand API Client".to_string(),
        }),
    };
    let service = TrustApiService { xand_api_client };

    // when
    let service_status = service.services();

    // then
    assert_eq!(service_status.len(), 1);
    assert_eq!(service_status[0].name(), "Fake Xand API Client");
    assert_eq!(service_status[0].health().await, ServiceStatus::Up);
}

#[tokio::test]
async fn services__correctly_reports_service_down() {
    // given
    let service_status = ServiceStatus::Down {
        details: "Unable to connect to Xand API".to_string(),
    };
    let xand_api_client = FakeXandApiClient {
        total_issuance: None,
        health: Some(Health {
            service_status: service_status.clone(),
            name: "Another Fake Xand API Client".to_string(),
        }),
    };
    let service = TrustApiService { xand_api_client };

    // when
    let all_the_status = service.services();

    // then
    assert_eq!(all_the_status.len(), 1);
    assert_eq!(all_the_status[0].name(), "Another Fake Xand API Client");
    assert_eq!(all_the_status[0].health().await, service_status);
}
