use super::*;
use crate::ServiceStatus;
use async_trait::async_trait;
use xand_api_client::Blockstamp;

pub struct Health {
    pub service_status: ServiceStatus,
    pub name: String,
}

pub struct FakeXandApiClient {
    pub total_issuance: Option<TotalIssuance>,
    pub health: Option<Health>,
}

#[async_trait]
impl XandApiService for FakeXandApiClient {
    async fn get_total_issuance(&self) -> Result<TotalIssuance> {
        Ok(self.total_issuance.clone().unwrap_or(TotalIssuance {
            total_issued: 0,
            blockstamp: Blockstamp {
                block_number: 0,
                unix_timestamp_ms: 0,
                is_stale: false,
            },
        }))
    }
}

#[async_trait]
impl IService for FakeXandApiClient {
    fn name(&self) -> String {
        self.health
            .as_ref()
            .map_or_else(|| "fake service".to_string(), |s| s.name.clone())
    }

    async fn health(&self) -> ServiceStatus {
        self.health
            .as_ref()
            .map_or_else(|| ServiceStatus::Up, |s| s.service_status.clone())
    }
}
