use crate::{
    i_trust_api::{
        unit_tests::{FakeTotalClaimsService, FakeTrustApi, TEST_TIMESTAMP},
        ITrustApiFactory,
    },
    web_server::{build_web_app, ServerState},
    ITrustApi, TotalClaims, TrustApiDoc,
};
use actix_web::{http::StatusCode, test, web::Data};
use chrono::DateTime;
use trust_api_config::ITrustApiConfig;
use utoipa::OpenApi;

#[derive(Clone)]
pub struct TestConfig {
    pub total_claims: Option<TotalClaims>,
}

impl ITrustApiConfig for TestConfig {
    fn port(&self) -> u16 {
        unimplemented!()
    }
}

impl ITrustApiFactory for TestConfig {
    fn generate_service(&self) -> Box<dyn ITrustApi> {
        let total_claims_service = self
            .total_claims
            .as_ref()
            .map_or_else(FakeTotalClaimsService::new_unresponsive, |total_claims| {
                FakeTotalClaimsService::new_with_total_claims(total_claims.clone())
            });
        Box::new(FakeTrustApi { total_claims_service })
    }
}

#[tokio::test]
async fn test_app__health_endpoint_ok() {
    // Given a config that generates a test trust api service
    let config = TestConfig { total_claims: None };
    let server_state = Data::new(ServerState {
        service: config.generate_service(),
    });
    let open_api = TrustApiDoc::openapi();

    // And an app instance with the given state
    let app = test::init_service(build_web_app(server_state.clone(), open_api.clone())).await;
    // And a request to the service health endpoint
    let req = test::TestRequest::get().uri("/service/health").to_request();

    // When the request is made against the app
    let resp = test::call_service(&app, req).await;

    // Then the response is healthy
    assert_eq!(resp.status(), StatusCode::OK);
}

#[tokio::test]
async fn test_app__total_claims_endpoint_errors_with_unresponsive_service() {
    // Given a config that generates a test trust api service that has an unresponsive total claims service
    let config = TestConfig { total_claims: None };
    let server_state = Data::new(ServerState {
        service: config.generate_service(),
    });
    let open_api = TrustApiDoc::openapi();

    // And an app instance containing the given state
    let app = test::init_service(build_web_app(server_state.clone(), open_api.clone())).await;
    // And a request to the total claims endpoint
    let req = test::TestRequest::get().uri("/total-claims").to_request();

    // When the request is made against the app
    let resp = test::call_service(&app, req).await;

    // An internal server error is returned
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
}

#[tokio::test]
async fn test_app__total_claims_endpoint_ok_with_valid_service() {
    // Given a config that generates a test trust api service that has a valid total claims service
    let config = TestConfig {
        total_claims: Some(TotalClaims {
            total_claims_in_minor_units: 1000,
            finalized_block_id: 100,
            timestamp: DateTime::parse_from_rfc3339(TEST_TIMESTAMP).unwrap().into(),
        }),
    };
    let server_state = Data::new(ServerState {
        service: config.generate_service(),
    });
    let open_api = TrustApiDoc::openapi();

    // And an app instance containing the given state
    let app = test::init_service(build_web_app(server_state.clone(), open_api.clone())).await;
    // And a request to the total claims endpoint
    let req = test::TestRequest::get().uri("/total-claims").to_request();

    // When the request is made against the app
    let resp = test::call_service(&app, req).await;

    // Then the response is healthy
    assert_eq!(resp.status(), StatusCode::OK);
}
