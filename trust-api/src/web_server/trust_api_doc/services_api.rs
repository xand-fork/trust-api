#![allow(clippy::use_self)]

use crate::{i_trust_api::i_service::ServiceStatus, web_server::ServerState};
use actix_web::{get, web::Data, HttpResponse};
use serde::Serialize;

#[derive(utoipa::Component, Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ServiceHealthResponse {
    pub up: Vec<HealthCheckSummary>,
    pub down: Vec<HealthCheckSummary>,
}

#[derive(utoipa::Component, Clone, Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct HealthCheckSummary {
    /// The name of the service.
    service: String,
    status: HealthCheckStatus,
    /// Details about the service status. This field is usually only populated if the service is down.
    #[serde(skip_serializing_if = "Option::is_none")]
    details: Option<String>,
}

#[derive(utoipa::Component, Clone, Debug, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub enum HealthCheckStatus {
    Up,
    Down,
}

/// Services health check
///
/// Returns service health status for components.
#[utoipa::path(
responses(
(status = 200, description = "Retrieved health check information.", body = ServiceHealthResponse),
(status = 500, description = "Internal Server Error."),
)
)]
#[get("/service/health")]
// This is only marked "must_use" to appease pedantic clippy rules.
// https://rust-lang.github.io/rust-clippy/master/index.html#must_use_candidate
pub async fn get_service_health(state: Data<ServerState>) -> HttpResponse {
    get_service_health_impl(state).await
}

async fn get_service_health_impl(state: Data<ServerState>) -> HttpResponse {
    let mut health_check_summaries = vec![];
    for service in state.service.services() {
        let health_status = service.health().await;
        let summary = match health_status {
            ServiceStatus::Up => HealthCheckSummary {
                service: service.name(),
                status: HealthCheckStatus::Up,
                details: None,
            },
            ServiceStatus::Down { details } => HealthCheckSummary {
                service: service.name(),
                status: HealthCheckStatus::Down,
                details: Some(details),
            },
        };
        health_check_summaries.push(summary);
    }
    let (up, down) = health_check_summaries
        .into_iter()
        .partition(|summary| summary.status == HealthCheckStatus::Up);
    let response = ServiceHealthResponse { up, down };
    HttpResponse::Ok().json(response)
}

#[cfg(test)]
mod unit_tests {
    use super::*;
    use crate::i_trust_api::{
        error::Result,
        i_service::{
            unit_tests::{AlwaysUpService, ExpectDownService},
            IService,
        },
        ITrustApi, TotalClaims,
    };
    use actix_web::http::StatusCode;
    use async_trait::async_trait;

    pub struct FakeTrustServicesApi {
        service_1: AlwaysUpService,
        service_2: ExpectDownService,
    }

    #[async_trait]
    impl ITrustApi for FakeTrustServicesApi {
        async fn get_total_claims(&self) -> Result<TotalClaims> {
            unimplemented!() // Fake is not used
        }

        fn services(&self) -> Vec<&dyn IService> {
            vec![&self.service_1, &self.service_2]
        }
    }

    #[tokio::test]
    async fn get_service_health_ok() {
        let trust_api = FakeTrustServicesApi {
            service_1: AlwaysUpService,
            service_2: ExpectDownService,
        };
        let state = Data::new(ServerState {
            service: Box::new(trust_api),
        });

        // When
        let response = get_service_health_impl(state).await;

        // Then
        assert_eq!(response.status(), StatusCode::OK);
    }
}
