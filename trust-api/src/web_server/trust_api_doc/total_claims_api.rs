use crate::{i_trust_api::TotalClaims, web_server::ServerState};
use actix_web::{get, web::Data, HttpResponse};
use chrono::{DateTime, Utc};
use serde::Serialize;

#[derive(utoipa::Component, Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TotalClaimsResponse {
    /// The total network claims (in USD minor units).
    pub total_claims_in_minor_units: u64,
    /// The block id of the finalized block reporting these total claims.
    pub finalized_block_id: u64,
    /// Datetime timestamp (in UTC) of the finalized block.
    pub timestamp: DateTime<Utc>,
}

impl From<TotalClaims> for TotalClaimsResponse {
    fn from(total_claims: TotalClaims) -> Self {
        Self {
            total_claims_in_minor_units: total_claims.total_claims_in_minor_units,
            finalized_block_id: total_claims.finalized_block_id,
            timestamp: total_claims.timestamp,
        }
    }
}

/// Get total claims
///
/// Returns the total claims on the network.
#[utoipa::path(
responses(
(status = 200, description = "Retrieved total claims information.", body = TotalClaimsResponse),
(status = 500, description = "Internal Server Error."),
)
)]
#[get("/total-claims")]
// This is only marked "must_use" to appease pedantic clippy rules.
// https://rust-lang.github.io/rust-clippy/master/index.html#must_use_candidate
pub async fn get_total_claims(state: Data<ServerState>) -> HttpResponse {
    get_total_claims_impl(state).await
}

async fn get_total_claims_impl(state: Data<ServerState>) -> HttpResponse {
    let total_claims_result = state.service.get_total_claims().await;

    match total_claims_result {
        Ok(response) => HttpResponse::Ok().json(TotalClaimsResponse::from(response)),
        Err(err) => HttpResponse::InternalServerError().json(err.to_string()),
    }
}

#[cfg(test)]
mod unit_tests {
    use super::*;
    use crate::i_trust_api::unit_tests::{FakeTotalClaimsService, FakeTrustApi, TEST_TIMESTAMP};
    use actix_web::http::{header::HeaderValue, StatusCode};

    #[tokio::test]
    async fn get_total_claims_ok() {
        // Given
        let total_claims_response = TotalClaims {
            total_claims_in_minor_units: 1000,
            finalized_block_id: 100,
            timestamp: DateTime::parse_from_rfc3339(TEST_TIMESTAMP).unwrap().into(),
        };
        let state = Data::new(ServerState {
            service: Box::new(FakeTrustApi {
                total_claims_service: FakeTotalClaimsService::new_with_total_claims(total_claims_response),
            }),
        });

        // When
        let response = get_total_claims_impl(state).await;

        // Then
        assert_eq!(
            response.headers().get("content-type"),
            Some(&HeaderValue::from_static("application/json"))
        );
        assert_eq!(response.status(), StatusCode::OK);
    }

    #[tokio::test]
    async fn get_total_claims_errors() {
        // Given
        let state = Data::new(ServerState {
            service: Box::new(FakeTrustApi {
                total_claims_service: FakeTotalClaimsService::new_unresponsive(),
            }),
        });

        // When
        let response = get_total_claims_impl(state).await;

        // Then
        assert_eq!(response.status(), StatusCode::INTERNAL_SERVER_ERROR);
    }
}
