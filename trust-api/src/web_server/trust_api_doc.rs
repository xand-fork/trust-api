pub mod services_api;
pub mod total_claims_api;

use serde::Serialize;
use services_api::{HealthCheckStatus, HealthCheckSummary, ServiceHealthResponse};
use total_claims_api::TotalClaimsResponse;
use utoipa::OpenApi;

#[derive(OpenApi, Debug, Serialize)]
#[openapi(
    handlers(total_claims_api::get_total_claims, services_api::get_service_health),
    components(TotalClaimsResponse, ServiceHealthResponse, HealthCheckStatus, HealthCheckSummary)
)]
pub struct TrustApiDoc;
