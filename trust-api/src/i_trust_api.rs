pub mod i_service;

pub mod error;
#[cfg(test)]
pub mod unit_tests;

use async_trait::async_trait;
use chrono::{DateTime, Utc};
use error::Result;
use i_service::IService;

/// An interface defining the expected functionality of a Trust API
#[async_trait]
pub trait ITrustApi: Send + Sync {
    /// Returns a response that contains the total claims on the network with meta data about the
    /// blockchain status from when they were fetched
    async fn get_total_claims(&self) -> Result<TotalClaims>;
    /// Returns the internal services relevant to the TrustApi
    fn services(&self) -> Vec<&dyn IService>;
}

/// Defines the interface necessary to generate a Trust API application
pub trait ITrustApiFactory: Send + Sync {
    /// Generates the Trust API service to be accessed by the app
    fn generate_service(&self) -> Box<dyn ITrustApi>;
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct TotalClaims {
    /// The total network claims (in USD minor units).
    pub total_claims_in_minor_units: u64,
    /// The block id of the finalized block reporting these total claims.
    pub finalized_block_id: u64,
    /// Datetime timestamp (in UTC) of the finalized block.
    pub timestamp: DateTime<Utc>,
}
