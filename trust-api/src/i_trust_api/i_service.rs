#[cfg(test)]
pub mod unit_tests;

use async_trait::async_trait;

/// An interface for component dependencies to report their health
#[async_trait]
pub trait IService: Send + Sync {
    fn name(&self) -> String;
    async fn health(&self) -> ServiceStatus;
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ServiceStatus {
    Up,
    Down { details: String },
}
