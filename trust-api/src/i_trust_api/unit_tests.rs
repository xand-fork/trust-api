use crate::i_trust_api::error::{Error, Result};
use crate::{i_trust_api::i_service::ServiceStatus, IService, ITrustApi, TotalClaims};
use assert_matches::assert_matches;
use async_trait::async_trait;
use chrono::DateTime;

pub const TEST_TIMESTAMP: &str = "2022-03-14T00:00:00Z";

#[derive(Default)]
pub struct FakeTotalClaimsService {
    total_claims: Option<TotalClaims>,
}

impl FakeTotalClaimsService {
    pub fn new_unresponsive() -> Self {
        Self::default()
    }

    pub const fn new_with_total_claims(total_claims: TotalClaims) -> Self {
        Self {
            total_claims: Some(total_claims),
        }
    }
}

#[async_trait]
impl IService for FakeTotalClaimsService {
    fn name(&self) -> String {
        "Fake Total Claims Service".to_string()
    }

    async fn health(&self) -> ServiceStatus {
        ServiceStatus::Up
    }
}

pub struct FakeTrustApi {
    pub total_claims_service: FakeTotalClaimsService,
}

#[async_trait]
impl ITrustApi for FakeTrustApi {
    async fn get_total_claims(&self) -> Result<TotalClaims> {
        self.total_claims_service
            .total_claims
            .clone()
            .ok_or(Error::ServiceUnresponsive)
    }

    fn services(&self) -> Vec<&dyn IService> {
        vec![&self.total_claims_service]
    }
}

#[tokio::test]
async fn get_total_claims__returns_expected_response() {
    // Given
    let expected_total_claims_response = TotalClaims {
        total_claims_in_minor_units: 1000,
        finalized_block_id: 100,
        timestamp: DateTime::parse_from_rfc3339(TEST_TIMESTAMP).unwrap().into(),
    };
    let trust_api = FakeTrustApi {
        total_claims_service: FakeTotalClaimsService::new_with_total_claims(expected_total_claims_response.clone()),
    };

    // When
    let actual_total_claims_response = trust_api.get_total_claims().await;

    // Then
    assert_matches!(actual_total_claims_response, Ok(total_claims_response) if total_claims_response == expected_total_claims_response);
}

#[tokio::test]
async fn get_total_claims__errors_when_service_unresponsive() {
    // Given
    let trust_api = FakeTrustApi {
        total_claims_service: FakeTotalClaimsService::new_unresponsive(),
    };

    // When
    let actual_total_claims_response = trust_api.get_total_claims().await;

    // Then
    assert_matches!(actual_total_claims_response, Err(total_claims_response) if matches!(&total_claims_response, Error::ServiceUnresponsive));
}
