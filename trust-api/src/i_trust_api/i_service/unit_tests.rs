use crate::{IService, ServiceStatus};
use async_trait::async_trait;

pub struct AlwaysUpService;

#[async_trait]
impl IService for AlwaysUpService {
    fn name(&self) -> String {
        "Magic Service".to_string()
    }

    async fn health(&self) -> ServiceStatus {
        ServiceStatus::Up
    }
}

pub struct ExpectDownService;

#[async_trait]
impl IService for ExpectDownService {
    fn name(&self) -> String {
        "Sad and Bad Service".to_string()
    }

    async fn health(&self) -> ServiceStatus {
        ServiceStatus::Down {
            details: "Insert reason service is down here".to_string(),
        }
    }
}

#[test]
fn name__returns_stringified_name() {
    // Given
    let down_service = ExpectDownService;
    let expected_name = "Sad and Bad Service".to_string();

    // When
    let actual_name = down_service.name();

    // Then
    assert_eq!(actual_name, expected_name);
}

#[tokio::test]
async fn health__returns_down_status_when_service_is_down() {
    // Given
    let down_service = ExpectDownService;
    let expected_status = ServiceStatus::Down {
        details: "Insert reason service is down here".to_string(),
    };

    // When
    let actual_status = down_service.health().await;

    // Then
    assert_eq!(actual_status, expected_status);
}

#[tokio::test]
async fn health__returns_up_status_when_service_is_up() {
    // Given
    let up_service = AlwaysUpService;
    let expected_status = ServiceStatus::Up;

    // When
    let actual_status = up_service.health().await;

    // Then
    assert_eq!(actual_status, expected_status);
}
