use std::result::Result as StdResult;
use thiserror::Error;
use xand_api_client::errors::XandApiClientError;

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Service Unresponsive")]
    ServiceUnresponsive,
    #[error(transparent)]
    InternalServiceError(#[from] Box<dyn std::error::Error + Send + Sync + 'static>),
}

impl From<XandApiClientError> for Error {
    fn from(e: XandApiClientError) -> Self {
        Self::InternalServiceError(Box::new(e))
    }
}
