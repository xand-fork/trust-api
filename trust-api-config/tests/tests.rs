use trust_api_config::{load_config, TrustApiConfig, XandApiConfig};

#[test]
#[allow(non_snake_case)]
fn load_config__returns_error_for_invalid_config() {
    // When
    let result = load_config("tests/invalid_config/invalid_config.yaml").unwrap_err();
    // Then
    let stringified_err = format!("{:?}", result);
    assert!(stringified_err.contains("xand_api"))
}

#[test]
#[allow(non_snake_case)]
fn load_config__returns_expected_structure_from_valid_config() {
    // Given
    let expected = TrustApiConfig {
        port: Some(9001),
        xand_api: XandApiConfig {
            url: "http://127.0.0.1:1000000".to_string(),
            jwt: "JWT".to_string(),
        },
    };
    // When
    let result = load_config("tests/valid_config/valid_config.yaml").unwrap();
    // Then
    assert_eq!(expected, result)
}
