use std::fmt::Debug;
use std::result::Result as StdResult;
use thiserror::Error;

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    ConfigError(Box<dyn std::error::Error + Send + Sync + 'static>),
    #[error(transparent)]
    GlobPatternError(Box<dyn std::error::Error + Send + Sync + 'static>),
}

impl From<config::ConfigError> for Error {
    fn from(e: config::ConfigError) -> Self {
        Self::ConfigError(Box::new(e))
    }
}

impl From<glob::PatternError> for Error {
    fn from(e: glob::PatternError) -> Self {
        Self::GlobPatternError(Box::new(e))
    }
}
