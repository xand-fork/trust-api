#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    clippy::unreadable_literal,
    rust_2018_idioms
)]
// Safety-critical application lints
#![deny(clippy::pedantic, clippy::float_cmp_const, clippy::unwrap_used)]
#![allow(
    clippy::implicit_return,
    clippy::iter_nth_zero,
    clippy::match_bool,
    clippy::missing_errors_doc,
    clippy::non_ascii_literal,
    clippy::wildcard_imports,
    incomplete_features
)]
// To use the `unsafe` keyword, do not remove the `unsafe_code` attribute entirely.
// Instead, change it to `#![allow(unsafe_code)]` or preferably `#![deny(unsafe_code)]` + opt-in
// with local `#[allow(unsafe_code)]`'s on a case-by-case basis, if practical.
#![forbid(unsafe_code, bare_trait_objects)]
// Uncomment before ship to reconcile use of possibly redundant crates, debug remnants, missing
// license files and more
// #![allow(clippy::blanket_clippy_restriction_lints)]
// #![warn(clippy::cargo, clippy::restriction, missing_docs, warnings)]
// #![allow(clippy::implicit_return)]
#![cfg_attr(test, allow(non_snake_case, clippy::unwrap_used))]

use config::{Config, File};
use serde::{Deserialize, Serialize};

mod error;
pub use error::{Error, Result};

/// Defines the configuration necessary to stand up a Trust API application
pub trait ITrustApiConfig: Send + Sync {
    /// Returns the port the config expects the web app to be hosted at
    fn port(&self) -> u16;
}

pub const DEFAULT_PORT: u16 = 8080;

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct XandApiConfig {
    pub url: String,
    pub jwt: String,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct TrustApiConfig {
    pub port: Option<u16>,
    pub xand_api: XandApiConfig,
}

impl ITrustApiConfig for TrustApiConfig {
    fn port(&self) -> u16 {
        self.port.unwrap_or(DEFAULT_PORT)
    }
}

pub fn load_config(config_path: &str) -> Result<TrustApiConfig> {
    let settings = Config::builder().add_source(File::with_name(config_path)).build()?;
    settings.try_deserialize().map_err(Error::from)
}
