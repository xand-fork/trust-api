#!/usr/bin/env bash

# Script meant to be run by CI to publish member crates one-by-one in dependent order for siblings
# If publishing is only partially successful, published workspace members may need to be yanked to be re-published by ci

trace-cmd.sh cargo-login cargo login --registry tpfs $TPFS_CRATES_TOKEN
trace-cmd.sh cargo-publish cargo publish --registry tpfs --manifest-path trust-api-config/Cargo.toml --allow-dirty
trace-cmd.sh cargo-publish cargo publish --registry tpfs --manifest-path trust-api/Cargo.toml --allow-dirty
