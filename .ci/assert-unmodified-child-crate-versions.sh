#!/bin/bash

set -e

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself#answer-53183593
DIR="$( realpath $( dirname "${BASH_SOURCE[0]}") )"

CONFIG_TOML="$DIR/../trust-api-config/Cargo.toml"

CONFIG_VERSION=$(toml get "$CONFIG_TOML" package.version | tr -d \")

if [ "$CONFIG_VERSION" != "0.0.0" ]; then
    echo "trust-api-config version has been manually modified: found \"$CONFIG_VERSION\", expected \"0.0.0\"."
    echo "Please reset the version of trust-api-config to \"0.0.0\". It is managed exclusively by CI."
    exit 1
fi