#!/bin/bash

# Sets the version of trust-api-config to match that of trust-api itself in your working tree.

set -e

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself#answer-53183593
DIR="$( realpath $( dirname "${BASH_SOURCE[0]}") )"

# Same args as "toml set": file name, key, value
toml_set() {
    NEW_CONTENTS=$(toml set "$1" $2 "$3")
    echo "$NEW_CONTENTS" > "$1"
}

MASTER_TOML="$DIR/../trust-api/Cargo.toml"
CONFIG_TOML="$DIR/../trust-api-config/Cargo.toml"

MASTER_VERSION=$(toml get "$MASTER_TOML" package.version | tr -d \")
toml_set "$CONFIG_TOML" package.version "$MASTER_VERSION"
toml_set "$MASTER_TOML" dependencies.trust-api-config.version "$MASTER_VERSION"