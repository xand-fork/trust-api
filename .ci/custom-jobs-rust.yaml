validate-config-used:
  extends: .shared-config
  stage: test
  before_script:
    - !reference [.trace-job-begin]
    - !reference [.crates-https-auth]
    - !reference [.crates-ssh-auth]
  script:
    - .ci/validate-config-use.sh
  rules:
    # This only needs to be checked during merge requests and non-release branches
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: never
    # Automatic job on other branches
    - if: '$CI_COMMIT_BRANCH'
      when: always
    # Exclude for non-branch pipeline types
    - when: never
  artifacts:
      name: ${CI_COMMIT_REF_SLUG}
      paths:
          - trust-api/build/

publish-openapi-beta:
  extends: .shared-config
  stage: publish
  variables:
    ARTIFACTORY_ARTIFACTS_URL: "https://transparentinc.jfrog.io/artifactory/artifacts-internal"
  before_script:
    - !reference [.trace-job-begin]
    - !reference [.crates-https-auth]
    - !reference [.crates-ssh-auth]
  when: manual
  script:
    - cd trust-api
    - cargo run -- --generate
    - export VERSION=$(toml get Cargo.toml package.version | tr -d '"')
    - export BETA_APPEND=beta-$CI_PIPELINE_IID
    - export BETA_VERSION=$VERSION-$BETA_APPEND
    - export ARTIFACT=build/trust-api-$VERSION.yaml
    - export BETA_ARTIFACT=trust-api-$BETA_VERSION.yaml
    - export PROPERTIES="job_url=$CI_JOB_URL;branch=$CI_COMMIT_REF_NAME;pipeline_id=$CI_PIPELINE_IID"
    - curl --fail -u$ARTIFACTORY_USER:$ARTIFACTORY_PASS -T $ARTIFACT "$ARTIFACTORY_ARTIFACTS_URL/trust_api/$BETA_ARTIFACT;$PROPERTIES"
  rules:
    # Never publish a beta openapi doc on the master branch
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: never
    # Manual job on other branches
    - if: '$CI_COMMIT_BRANCH'
      when: manual
    # Exclude for non-branch pipeline types
    - when: never
  artifacts:
      name: ${CI_COMMIT_REF_SLUG}
      paths:
          - trust-api/build/

publish-openapi:
  extends: .shared-config
  stage: publish
  variables:
    ARTIFACTORY_ARTIFACTS_URL: "https://transparentinc.jfrog.io/artifactory/artifacts-internal"
  before_script:
    - !reference [.trace-job-begin]
    - !reference [.crates-https-auth]
    - !reference [.crates-ssh-auth]
  script:
    - cd trust-api
    - cargo run -- --generate
    - export VERSION=$(toml get Cargo.toml package.version | tr -d '"')
    - export ARTIFACT=build/trust-api-$VERSION.yaml
    - export PROPERTIES="job_url=$CI_JOB_URL;branch=$CI_COMMIT_REF_NAME;pipeline_id=$CI_PIPELINE_IID"
    - curl --fail -u$ARTIFACTORY_USER:$ARTIFACTORY_PASS -T $ARTIFACT "$ARTIFACTORY_ARTIFACTS_URL/trust_api/trust-api-$VERSION.yaml;$PROPERTIES"
  rules:
    # Always publish an openapi doc on the master branch
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: on_success
    # Inaccessible job on other branches
    - if: '$CI_COMMIT_BRANCH'
      when: never
    # Exclude for non-branch pipeline types
    - when: never
  artifacts:
      name: ${CI_COMMIT_REF_SLUG}
      paths:
          - trust-api/build/

.publish-docker:
  stage: publish
  image: "gcr.io/xand-dev/rust-base-img:7.14.0"
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://localhost:2375
  services:
    - docker:18.09-dind
  tags:
    - kubernetes-runner
  # Gets artifacts at .docker/tmp/
  needs:
    - job: build
      artifacts: true
    - job: test
    - job: lint
  before_script:
    - docker login -u _json_key -p "$(echo $GCLOUD_DEV_SERVICE_KEY | base64 -d)" https://gcr.io
  script:
    - cd .docker
    - make debug-gitlab
    - make build
    - make publish

publish-docker-beta:
  extends: .publish-docker
  when: manual
  except:
    - master
    - tags

publish-docker:
  extends: .publish-docker
  only:
    - tags
  except:
    - branches
