#!/usr/bin/env bash

# Script meant to be run by CI to publish member crates one-by-one in dependent order for siblings with beta suffixes
# If publishing is only partially successful, published workspace members may need to be yanked to be re-published by ci

trace-cmd.sh cargo-login cargo login --registry tpfs $TPFS_CRATES_TOKEN
# Trust Api Config - Update Beta Version
cd trust-api-config
CURRENT_TRUST_API_CONFIG_VERSION=$(toml get Cargo.toml package.version | tr -d '"')
BETA_TRUST_API_CONFIG_VERSION="$CURRENT_TRUST_API_CONFIG_VERSION-beta.${CI_PIPELINE_IID}"
echo "Setting beta version in Cargo.toml -> $BETA_TRUST_API_CONFIG_VERSION"
NEW_CARGO_TOML=$(toml set Cargo.toml package.version $BETA_TRUST_API_CONFIG_VERSION)
echo "$NEW_CARGO_TOML" > Cargo.toml

# Trust Api - Update Beta Version + Set Config Version to Current Beta
cd ../trust-api
CURRENT_VERSION=$(toml get Cargo.toml package.version | tr -d '"')
BETA_VERSION="$CURRENT_VERSION-beta.${CI_PIPELINE_IID}"
echo "Setting beta version in Cargo.toml -> $BETA_VERSION"
NEW_CARGO_TOML=$(toml set Cargo.toml package.version $BETA_VERSION)
echo "$NEW_CARGO_TOML" > Cargo.toml
UPDATED_CONFIG_REFERENCE=$(toml set Cargo.toml dependencies.trust-api-config.version $BETA_TRUST_API_CONFIG_VERSION)
echo "$UPDATED_CONFIG_REFERENCE" > Cargo.toml

# Publish (in order of sibling dependency)
cd ..
trace-cmd.sh cargo-publish cargo publish --registry tpfs --manifest-path trust-api-config/Cargo.toml --allow-dirty
trace-cmd.sh cargo-publish cargo publish --registry tpfs --manifest-path trust-api/Cargo.toml --allow-dirty
