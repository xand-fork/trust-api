#!/usr/bin/env bash

# Script meant to be run by CI to ensure that the trust-api is using the latest config
cd trust-api-config
CURRENT_TRUST_API_CONFIG_VERSION=$(toml get Cargo.toml package.version | tr -d '"')
echo "Current Trust API Config: $CURRENT_TRUST_API_CONFIG_VERSION"
cd ../trust-api
USED_TRUST_API_CONFIG_VERSION=$(toml get Cargo.toml dependencies.trust-api-config.version | tr -d '"')
echo "Used Trust API Config: $USED_TRUST_API_CONFIG_VERSION"
if [ "$CURRENT_TRUST_API_CONFIG_VERSION" == "$USED_TRUST_API_CONFIG_VERSION" ]; then
    echo "Current config is used in Trust API"
else
    echo "Current config is not used in Trust API!"
    exit 1
fi