# Generated trust-api typescript client

## Dependencies

In order to build, you must have [`openapi-generator`](https://github.com/OpenAPITools/openapi-generator)
installed. This is an npm package which depends on java under the covers.

    sudo apt-get install default-jdk
    npm install -g openapi-generator

## Generated client
The Trust API generated client is defined in the SwaggerDoc (`/trust-api/build/trust-api-*.yaml`)

If the SwaggerDoc file should change, you can regenerate the SwaggerDoc and the
client by running:

    cd ../trust-api && cargo run -- --generate
    cd ../client && make build
