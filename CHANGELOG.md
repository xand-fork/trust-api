# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2022-05-20
* Implement entrypoint to the service
* Implement `XandApiService`, `IService` traits
* Implemented Xand API adapter `get_total_claims()`
* Implemented converter between `TotalIssuance` to `TotalClaims`
* Implemented `health()` check for `XandApiService` to check connectivity 
* Added intermediary trait `ITrustApi` for `TrustApiService` - using the `XandApiService` trait defined in xand-api-client

## [0.2.0] - 2022-05-04

### Added
* `ITrustApi`, `IService`, and `ITrustApiConfig` ports and implemented the existing endpoints against the interfaces.
* Functionality to serve up the swagger ui with an actix web app.

## [0.1.0] - 2022-04-25

### Added
* `total_claims_api` module for total claims types/handlers.
* `services_api` module for service health check types/handlers.
* [utoipa](https://github.com/juhaku/utoipa) boilerplate added for generating OpenAPI documentation
* basic examples showing swagger-ui, json and yaml formatting of OpenAPI struct.

## [Unreleased]
<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->



<!-- When a new release is made, add it here
e.g. ## [0.8.2] - 2020-11-09 -->
[Unreleased]: https://gitlab.com/TransparentIncDevelopment/product/libs/trust-api/-/compare/0.1.0...master
